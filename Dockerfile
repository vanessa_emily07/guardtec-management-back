FROM python:3.9-alpine

# RUN PYTHON WITHOUT BUFFER
ENV PYTHONUNBUFFERED 1

# INSTALL LIBSSL-DEV, GCC, MARIADB-CLIENT AND MARIADB-DEV
RUN apk add libressl-dev build-base mariadb-client mariadb-dev

# Copy our requirements file to the container
COPY ./requirements.txt /requirements.txt

# Install all dependecies that the requirements file has.
RUN pip install -r /requirements.txt

# Save our application
RUN mkdir /guardtec
WORKDIR  /guardtec
COPY ./guardtec /guardtec

# Create a user that is going to run our application using docker
RUN adduser -D user
USER user