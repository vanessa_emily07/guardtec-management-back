# GUARDTEC-MANAGEMENT-BACK

Project which allow manager manages residents and residences for the Manager platform.

# DEVELOPMENT ENVIRONMENT

**Requirements:**

- docker
- docker-compose
- MariaDB

**Instructions**

1. Download the project from https://gitlab.com/vanessa_emily07/guardtec-management-back

```
git clone https://gitlab.com/vanessa_emily07/guardtec-management-back.git

```

2. Go to the directory 

```
cd guardtec-management-back
```

3. Build Image

```
docker build .
```

4. Build docker compose service.

```
docker-compose build
```

5. Copy the **.env.example** file to **.env**

```
cp .env.example .env
```

6. Open the **.env** file and configure the DATABASE, EMAIL, FRONT-END REDIRECT TO CHANGE PASSWORD, FILENAME OF REPORTS and DJANGO SETTINGS with the correct data 

```
# DJANGO SETTINGS
SECRET_KEY=''

# DATABASE VARIABLES
DATABASE_NAME=''
DATABASE_USER=''
DATABASE_PASSWORD=''
DATABASE_HOST=''
DATABASE_PORT=3306

# EMAIL VARIABLES
EMAIL_USE_TLS=True
EMAIL_HOST='smtp.sendgrid.net'
EMAIL_HOST_USER=''
EMAIL_HOST_PASSWORD=''
EMAIL_PORT=587
DEFAULT_FROM_EMAIL=''

# FRONT-END REDIRECT TO CHANGE PASSWORD
SITE_DOMAIN=''
URL_CHANGE_PASSWORD='cambio-password'

# FILENAME OF REPORTS
RESIDENCES_REPORT_FILENAME='reporte-residencias.xlsx'
```

7. Run docker-compose.

```
docker-compose up
```

8. In order to run migrations, we need to enter the **guardtec-management-back_guardtec** container.

    Open a new terminal and run the following comand

    ```
    docker ps
    ```

    The output will be a list of all containers that are up with the following columns names: Container ID, image, command, created, status, ports and names. We need to copy the **container id** of the **guardtec-management-back_guardtec** then we run the following command:

    ```
        docker exec -it CONTAINER_ID  sh
    ```

    Where CONTAINER_ID is the identifier that we copied. When we are in the container, we can run migrations:

    ```
    python manage.py migrate
    ```

9. You can check in your local. Type  _127.0.0.1:8000_ in your browser

# DEVELOPMENT ENVIRONMENT WITHOUT DOCKER

**Requirements:**

- python v3.8+
- pip v3
- MariaDB

- A database for the project 

**Instructions**

1. Download the project from https://git.gtec.com.mx/gtec/guardtec-management-back.git

```
git clone https://git.gtec.com.mx/gtec/guardtec-management-back.git
```

2. Go to the directory Guardtec that it's inside the main directory of the project 

```
cd guardtec-management-back/guardtec
```

3. Install dependecies

```
pip3 install -r ../requirements.txt
```

4. Copy the **.env.example** file to **.env**

```
cp .env.example .env
```

5. Open the **.env** file and configure the DATABASE, EMAIL, FRONT-END REDIRECT TO CHANGE PASSWORD, FILENAME OF REPORTS and DJANGO SETTINGS with the correct data 

```
# DJANGO SETTINGS
SECRET_KEY=''

# DATABASE VARIABLES
DATABASE_NAME=''
DATABASE_USER=''
DATABASE_PASSWORD=''
DATABASE_HOST=''
DATABASE_PORT=3306

# EMAIL VARIABLES
EMAIL_USE_TLS=True
EMAIL_HOST=''
EMAIL_HOST_USER=''
EMAIL_HOST_PASSWORD=''
EMAIL_PORT=587
DEFAULT_FROM_EMAIL=''

# FRONT-END REDIRECT TO CHANGE PASSWORD
SITE_DOMAIN=''
URL_CHANGE_PASSWORD='cambio-password'

# FILENAME OF REPORTS
RESIDENCES_REPORT_FILENAME='reporte-residencias.xlsx'
```

6. Run migrations

```
python3 manage.py migrate
```

7. Run app

```
python3 manage.py runserver 0.0.0.0:8000
```

### SERVICES
To check services that are available, we can check it in the following URL *http://localhost:8000/redoc* 

**Note: The IP address can change for different environments** 

