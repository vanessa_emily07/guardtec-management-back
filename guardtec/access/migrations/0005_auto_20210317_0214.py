# Generated by Django 3.1.7 on 2021-03-17 02:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trust_visitors', '0001_initial'),
        ('access', '0004_auto_20210317_0131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='access',
            name='trust_visitor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='trust_visitor', to='trust_visitors.trustvisitor'),
        ),
    ]
