from django.db import models
from datetime import date

from domestic_servant_visitors.models import DomesticServantVisitor
from hut_visitors.models import HutVisitor
from trust_visitors.models import TrustVisitor
from access_automatic_visitors.models import AccessAutomaticVisitor
from suppliers_visitors.models import SupplierVisitor
from vigilants.models import Vigilant
from identifier_types.models import IdentifierType
from pedestrian_access.models import PedestrianAccess

class Access(models.Model):
    entry_date = models.DateField(auto_now_add=False, default=date.today)
    exit_date = models.DateField(auto_now_add=False, null=True)
    license_plate = models.CharField(max_length=10, null=True)
    picture_url = models.CharField(max_length=255)
    identifier_picture_url = models.CharField(max_length=255)
    second_identifier_picture_url = models.CharField(max_length=255)
    suitcase_number = models.CharField(max_length=45, null=True)
    domestic_servant_visitors = models.ForeignKey(DomesticServantVisitor, on_delete=models.CASCADE, null=True)
    hut_visitors = models.ForeignKey(HutVisitor, on_delete=models.CASCADE, null=True)
    trust_visitor = models.ForeignKey(TrustVisitor, on_delete=models.CASCADE, null=True)
    access_automatic_visitor = models.ForeignKey(AccessAutomaticVisitor, on_delete=models.CASCADE, null=True)
    supplier_visitor = models.ForeignKey(SupplierVisitor, on_delete=models.CASCADE, null=True)
    vigilant = models.ForeignKey(Vigilant, on_delete=models.CASCADE)
    identifier_type = models.ForeignKey(IdentifierType, on_delete=models.CASCADE)
    pedestrian_access = models.ForeignKey(PedestrianAccess, on_delete=models.CASCADE, null=True)    
    
    class Meta:
        ordering = ('id',)
    
    def __str__(self) -> str:
        return f"{self.entry_date}, {self.exit_date}, {self.license_plate}, {self.suitcase_number}"
