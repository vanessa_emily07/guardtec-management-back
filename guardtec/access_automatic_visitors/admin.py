from django.contrib import admin

from .models import AccessAutomaticVisitor

admin.site.register(AccessAutomaticVisitor)
