from django.apps import AppConfig


class AccessAutomaticVisitorsConfig(AppConfig):
    name = 'access_automatic_visitors'
