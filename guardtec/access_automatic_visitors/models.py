from django.db import models

from schedules.models import Schedule
from residents.models import Resident
from events.models import Event

class AccessAutomaticVisitor(models.Model):
    name = models.CharField(max_length=255, blank=False)
    date = models.DateField(auto_now_add=False)
    phone_number = models.CharField(max_length=15, blank=False)
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id',)
    
    def __str__(self) -> str:
        return f"{self.name}, {self.date}"