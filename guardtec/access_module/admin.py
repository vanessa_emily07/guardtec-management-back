from django.contrib import admin

from .models import AccessModule

admin.site.register(AccessModule)