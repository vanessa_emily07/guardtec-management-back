from django.apps import AppConfig


class AccessModuleConfig(AppConfig):
    name = 'access_module'
