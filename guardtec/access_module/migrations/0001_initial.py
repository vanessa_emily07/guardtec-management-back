# Generated by Django 3.1.7 on 2021-03-02 01:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('residential_group', '0002_remove_residentialgroup_administrators'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessModule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('annual_cost', models.DecimalField(decimal_places=2, default=0, max_digits=9)),
                ('monthly_cost', models.DecimalField(decimal_places=2, default=0, max_digits=9)),
                ('visitors_allowed', models.PositiveIntegerField()),
                ('residents_allowed', models.PositiveIntegerField()),
                ('domestic_servants_allowed', models.PositiveIntegerField()),
                ('created_at', models.DateField(auto_now=True)),
                ('residential_group', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='residential_group.residentialgroup')),
            ],
        ),
    ]
