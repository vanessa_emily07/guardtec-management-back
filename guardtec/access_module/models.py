from django.db import models

from residential_group.models import ResidentialGroup

class AccessModule(models.Model):
    annual_cost = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    monthly_cost = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    visitors_allowed = models.PositiveIntegerField()
    residents_allowed = models.PositiveIntegerField()
    domestic_servants_allowed = models.PositiveIntegerField()
    created_at = models.DateField(auto_now=True)
    residential_group = models.OneToOneField(ResidentialGroup, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.annual_cost}, {self.monthly_cost}, {self.visitors_allowed}, {self.residents_allowed}, {self.domestic_servants_allowed}, {self.created_at}"
