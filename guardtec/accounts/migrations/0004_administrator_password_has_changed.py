# Generated by Django 3.1.7 on 2021-03-08 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_administrator_residential_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='administrator',
            name='password_has_changed',
            field=models.BooleanField(default=False),
        ),
    ]
