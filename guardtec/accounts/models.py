from django.db import models
from django.contrib.auth.models import (
    AbstractUser, BaseUserManager
)

from residential_group.models import ResidentialGroup

class AdministratorManager(BaseUserManager):

    def create_user(self, username, email, name, last_name, password, residential_group):
        """
        Creates and saves a Administrator with the given username, email, name, last_name, password.
        """
        if not username:
            raise ValueError('Administrator must have a username.')
        if not email:
            raise ValueError('Administrator must have an email address.')
        if not password:
            raise ValueError('Administrator must have a password')
        if not name:
            raise ValueError('Administrator must have a name.')
        if not last_name:
            raise ValueError('Administrator must have a last name.')
        if not residential_group:
            raise ValueError('Administrator must have a residential  group')
        
        user = self.model(
            username=username,
            email=self.normalize_email(email),
            name=name,
            last_name=last_name,
            residential_group=residential_group,
            password_has_changed=False
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, email, password, name, last_name):
        """
        Create and save a User with superuser (admin) permissions.
        """
        if password is None:
            raise TypeError('Superusers must have a password')
        
        user = self.create_user(username, email, name, last_name, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return UnicodeTranslateError

class Administrator(AbstractUser):
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=False)
    residential_group = models.ForeignKey(ResidentialGroup, on_delete=models.CASCADE)
    password_has_changed = models.BooleanField(default=False)

    is_staff = models.BooleanField(default=False)

    REQUIRED_FIELDS = ['email', 'name', 'last_name', 'residential_group']

    objects = AdministratorManager()

    def __str__(self):
        return f"{self.username} {self.email} {self.name} {self.last_name} {self.residential_group} {self.password_has_changed}"