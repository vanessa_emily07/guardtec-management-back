from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from dj_rest_auth.serializers import PasswordResetSerializer

from accounts import models

class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = models.Administrator
        fields = ('password',)

    def validate(self, validated_data):
        try:
            validate_password(validated_data['password'])
        except ValidationError as e:
            raise serializers.ValidationError(e)
        return validated_data

    def update(self, instance, validated_data):

        user = self.context['request'].user

        if user.pk != instance.pk:
            raise serializers.ValidationError({"authorize": "You dont have permission for this user"})

        instance.set_password(validated_data['password'])
        instance.password_has_changed=True
        instance.save()

        return instance

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    """Login Serializer to get tokens and the current user."""
    
    def validate(self, attrs):
        data = super().validate(attrs)
        user = self.user
        refresh = self.get_token(self.user)
        data['lifetime'] = int(refresh.access_token.lifetime.total_seconds())
        serialized = AdministratorSerializer(user)
        data['user'] = serialized.data

        return data


class AdministratorSerializer(serializers.ModelSerializer):
    """
    Serializing Administrator
    """

    class Meta:
        model = models.Administrator
        fields = ('id', 'username', 'email', 'name', 'last_name', 'password_has_changed')


class RegisterSerializer(serializers.ModelSerializer):
    """Serializes an administrator object to register"""

    class Meta:
        model = models.Administrator
        fields = ('id','username', 'email', 'name', 'last_name', 'password', 'residential_group')
        extra_kwargs = {
            'password': {
                'write_only': True, # Only to create or update objects, we are not going to retrieve 
                'required': True,
                'min_length': 8,
                'max_length': 100,
                'style': {
                    'input_type': 'password'
                }
            },
            'name': { 'required': True },
            'last_name': { 'required': True },
            'email': { 'required': True },
            'username': { 'required': True },
            'residential_group': { 'required': True }
        }
    
    def validate(self, validated_data):
        try:
            validate_password(validated_data['password'])
        except ValidationError as e:
            raise serializers.ValidationError(e)
        return validated_data

    def create(self, validated_data):
        """Create and return a new administrator"""

        user = models.Administrator.objects.create_user(
            username = validated_data['username'],
            email = validated_data['email'],
            name = validated_data['name'],
            last_name = validated_data['last_name'],
            password = validated_data['password'],
            residential_group = validated_data['residential_group']
        )

        serialized = AdministratorSerializer(user)
        return serialized.data

class MyPasswordResetSerializer(PasswordResetSerializer):
    """
    Serializer for requesting a password reset e-mail.
    """

    def get_email_options(self):
        """Override this method to change default e-mail options"""
        return {
            'email_template_name': 'password_reset_email.html'
        }
