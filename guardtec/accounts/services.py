from .models import Administrator

def get_residential_group_by_administrator_id(administrator_id):
    """Get the residential group by administrator identifier"""

    administrator = Administrator.objects.get(pk=administrator_id)

    if administrator == None:
        return None

    return administrator.residential_group_id