import os
from dotenv import load_dotenv
from django import template

register = template.Library()

@register.simple_tag
def site_domain():
    load_dotenv()
    return os.environ.get("SITE_DOMAIN", '')

@register.simple_tag
def url_change_password():
    load_dotenv()
    return os.environ.get("URL_CHANGE_PASSWORD", '')

