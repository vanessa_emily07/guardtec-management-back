from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status  # HTTP STATUS 
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenViewBase
from rest_framework.permissions import AllowAny, IsAuthenticated

from accounts import serializers, models

class ChangePasswordView(generics.UpdateAPIView):
    """Endpoint to change the password the administrator that has been authenticated"""
    permission_classes = [IsAuthenticated]
    queryset = models.Administrator.objects.all()
    serializer_class = serializers.ChangePasswordSerializer
    
class LogoutView(APIView):
    """Endpoint to logout"""

    permission_classes = (IsAuthenticated,)
    
    def post(self, request):
        """Endpoint to logout an administrator and invalid token.s"""
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    

class LoginView(TokenViewBase):
    """
    Endpoint to login an administrator by their username and password, and return the access_token, refresh_token and information of the administrator.
    """

    permission_classes = (AllowAny,)
    serializer_class = serializers.MyTokenObtainPairSerializer


class RegisterApiView(generics.GenericAPIView):
    """Register API View"""

    permission_classes = (AllowAny,)
    serializer_class = serializers.RegisterSerializer

    def post(self, request):
        """
        Endpoint to create a new administrator by their username, email, name, last name, password and residential group identifier.
        And it returns the administrator information
        """

        serializers = self.serializer_class(data=request.data)

        if serializers.is_valid():
            username = serializers.validated_data.get('username')
            email = serializers.validated_data.get('email')
            name = serializers.validated_data.get('name')
            last_name = serializers.validated_data.get('last_name')
            password = serializers.validated_data.get('password')
            residential_group = serializers.validated_data.get('residential_group')
            
            user = serializers.save()
            return Response({
                'message': 'Administrator created successfully',
                'user': user
            })
        else:
            return Response(
                serializers.errors, 
                status=status.HTTP_400_BAD_REQUEST
            )
