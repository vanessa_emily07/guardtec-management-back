from django.contrib import admin

from .models import SuperAdministrator

admin.site.register(SuperAdministrator)