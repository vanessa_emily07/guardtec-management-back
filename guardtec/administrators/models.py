from django.db import models

class SuperAdministrator(models.Model):
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=False)
    password = models.CharField(max_length=100)    

    def __str__(self):
        return f"{self.username} {self.email} {self.name} {self.last_name}"