from django.contrib import admin

from .models import ApplicationAccess

admin.site.register(ApplicationAccess)