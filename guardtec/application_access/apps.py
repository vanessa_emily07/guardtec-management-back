from django.apps import AppConfig


class ApplicationAccessConfig(AppConfig):
    name = 'application_access'
