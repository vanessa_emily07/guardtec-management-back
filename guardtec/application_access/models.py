from django.db import models

class ApplicationAccess(models.Model):
    name = models.CharField(max_length=150, blank=False)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name