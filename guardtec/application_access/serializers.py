from rest_framework import serializers

from .models import ApplicationAccess

class ApplicationAccessSerializer(serializers.ModelSerializer):
    """Serializing Application Access"""
    
    class Meta:
        model = ApplicationAccess
        fields = ('id', 'name')

class SaveApplicationAccessSerializer(serializers.ModelSerializer):
    """
    Serializes an application access.
    """

    class Meta:
        model = ApplicationAccess
        fields = ('id','name')
        extra_kwargs = {
            'name': { 'required': True, 'max_length': 45, 'min_length': 3 }
        }

    def create(self, validated_data):
        """Create and return an application access object"""

        application_access = ApplicationAccess.objects.create(
            name = validated_data['name']
        ) 
        
        serialized = ApplicationAccessSerializer(application_access)
        return serialized.data