from .models import ApplicationAccess

def get_application_access_by_id(application_access_id):
    """Get application access by identifier"""
    
    try: 
        return ApplicationAccess.objects.filter(pk=application_access_id).get()
    except:
        return None