from django.urls import path
from django.urls.resolvers import URLPattern

from .views import ApplicationAccessAPIView

urlpatterns = [
    path('application-access', ApplicationAccessAPIView.as_view())
]