from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response

from .serializers import SaveApplicationAccessSerializer, ApplicationAccessSerializer
from .models import ApplicationAccess

class ApplicationAccessAPIView(generics.GenericAPIView):
    """Application access API View"""

    permission_classes = [IsAuthenticated]
    serializer_class = SaveApplicationAccessSerializer

    def get(self, request):
        """Returns a list of the catalog application access."""

        application_status = ApplicationAccess.objects.all()
        serializer = ApplicationAccessSerializer(application_status, many=True)
        return Response({
            'application_status': serializer.data
        })

    def post(self, request):
        """Create an element in the catalog application access"""
        
        serializers = self.serializer_class(data=request.data)

        if serializers.is_valid():
            name = serializers.validated_data.get('name')
            application_access = serializers.save()
            return Response({
                'message': 'Visiting status created successfully',
                'application_access': application_access
            })
        else:
            return Response(
                serializers.errors, 
                status=status.HTTP_400_BAD_REQUEST
            )
