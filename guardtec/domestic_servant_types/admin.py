from django.contrib import admin

from .models import DomesticServantType

admin.site.register(DomesticServantType)