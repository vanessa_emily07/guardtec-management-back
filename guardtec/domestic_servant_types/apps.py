from django.apps import AppConfig


class DomesticServantTypesConfig(AppConfig):
    name = 'domestic_servant_types'
