from django.db import models

class DomesticServantType(models.Model):
    name = models.CharField(max_length=50, blank=False)

    class Meta:
        ordering = ('name',)

    def __str__(self) -> str:
        return self.name