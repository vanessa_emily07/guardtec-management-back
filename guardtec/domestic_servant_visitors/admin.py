from django.contrib import admin

from .models import DomesticServantVisitor

admin.site.register(DomesticServantVisitor)