from django.apps import AppConfig


class DomesticServantVisitorsConfig(AppConfig):
    name = 'domestic_servant_visitors'
