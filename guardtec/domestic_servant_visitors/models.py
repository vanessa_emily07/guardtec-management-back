from django.db import models
from datetime import date

from residents.models import Resident
from domestic_servant_types.models import DomesticServantType

class DomesticServantVisitor(models.Model):
    name = models.CharField(max_length=255, blank=False)
    phone_number = models.CharField(max_length=15, blank=False)
    picture_url = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=False, default=date.today)
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    domestic_servant_type = models.ForeignKey(DomesticServantType, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id',)
    
    def __str__(self) -> str:
        return f"{self.name}, {self.phone_number}, {self.date}"