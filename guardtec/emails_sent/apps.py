from django.apps import AppConfig


class EmailsSentConfig(AppConfig):
    name = 'emails_sent'
