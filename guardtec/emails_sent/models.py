from django.db import models
from datetime import date

from residents.models import Resident

class EmailSent(models.Model):
    date_send = models.DateField(auto_now_add=False, default=date.today)
    resident = models.OneToOneField(Resident, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.date_send}, {self.resident}"