from django.core import mail
import os

def send_emails(residents, residence_name):
    """Send messages to all residents"""
    connection = mail.get_connection()
    DEFAULT_FROM_EMAIL = os.getenv("DEFAULT_FROM_EMAIL")
    
    connection.open()
    
    list_emails = []

    for resident in residents:
        html_content = f'''
                <p>Estimad(a) {resident.name} {resident.last_name}.</p>
                <p>Se le ha otorgado acceso al sistema G-Residencial Seguro para el residencial {residence_name}</p>
                <p>Por favor descárguelo en:</p>
                <ul>
                    <li>App store</li>
                    <li>Play store</li>
                    </ul>
                <p>¡Y comience a tener una residencia más segura!</p>'''

        email = mail.EmailMessage(
            'Bienvenido al sistema de G-Residencial Seguro',
            html_content,
            DEFAULT_FROM_EMAIL,
            [resident.email],
            connection=connection,
        )
        email.content_subtype = "html"

        list_emails.append(email)
    
    connection.send_messages(list_emails)
    connection.close()
