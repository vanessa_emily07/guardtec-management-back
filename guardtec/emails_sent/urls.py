from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import SendResidentsAccessAPIView

urlpatterns = [
    path('residents/send-access', SendResidentsAccessAPIView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)