from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status


from residences.models import Residence
from residents.models import Resident
from resident_types.services import get_administrator_or_contact_resident_type

from .models import EmailSent
from .services import send_emails

class SendResidentsAccessAPIView(APIView):
    """Send residents access view"""

    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        """Send an email to all residents to get all access."""

        user = self.request.user
        queryset_residences = Residence.objects.filter(residential_group = user.residential_group)
        list_ids = [residence.id for residence in queryset_residences]
        
        # Get administrator or contact resident type identifier
        queryset_resident_types = get_administrator_or_contact_resident_type()

        if queryset_resident_types is not None:
            return Response({'message: ' 'There is not any resident type'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        list_resident_types_ids = [resident_type.id for resident_type in queryset_resident_types]
        
        # Get all residents which are active and has the resident type valid.
        queryset_residents = Resident.objects.filter(resident_type__in=list_resident_types_ids, residence__in=list_ids, is_active=True)
        list_residents_ids = [resident.id for resident in queryset_residents]

        # Get all residents where it has not sent emails
        queryset_email_sent = EmailSent.objects.filter(resident__in=list_residents_ids)
        queryset_emails_not_sent = queryset_residents.exclude(pk__in=queryset_email_sent)
        
        send_emails(queryset_emails_not_sent, user.residential_group.name)
        for resident in queryset_emails_not_sent:
            emailSent = EmailSent(resident=resident)
            emailSent.save()

        return Response({ 'message': 'Emails sent successfully' })