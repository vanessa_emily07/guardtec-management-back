from django.db import models

from schedules.models import Schedule
from residents.models import Resident

class Event(models.Model):
    name = models.CharField(max_length=255, blank=False)
    date = models.DateField(auto_now_add=False)
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id', )

    def __str__(self):
        return f"{self.street}, {self.external_number}, {self.kind_of_residence}, {self.internal_number}"