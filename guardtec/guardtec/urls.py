"""guardtec URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework_simplejwt import views as jwt_views
from dj_rest_auth.views import PasswordResetView, PasswordResetConfirmView
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

from accounts.views import LoginView, LogoutView, ChangePasswordView

schema_view = get_schema_view(
    openapi.Info(
        title="GUARDTEC ADMINISTRATOR MANAGEMENT API",
        default_version='v1',
        description="Project which allow manager manage residents and residences for the Manager platform.",
        contact=openapi.Contact(email="vanessa.garcia@gtec.com.mx"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('admin/', admin.site.urls),
    path('api/v1/login', LoginView.as_view(), name='token_obtain'),
    path('api/v1/login/refresh', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/', include('accounts.urls')),
    path('api/v1/', include('visiting_status.urls')),
    path('api/v1/', include('residence_status.urls')),
    path('api/v1/', include('resident_types.urls')),
    path('api/v1/', include('application_access.urls')),
    path('api/v1/', include('residences.urls')),
    path('api/v1/', include('residents.urls')),
    path('api/v1/', include('statics.urls')),
    path('api/v1/', include('emails_sent.urls')),
    path('api/v1/logout', LogoutView.as_view(), name='auth_logout'),
    path('api/v1/change_password/<int:pk>', ChangePasswordView.as_view(), name='auth_change_password'),
    path('api/v1/password-reset', PasswordResetView.as_view()),
    path('api/v1/password-reset-confirm/<uidb64>/<token>', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),  
]