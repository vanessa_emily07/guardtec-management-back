from django.apps import AppConfig


class HutVisitorsConfig(AppConfig):
    name = 'hut_visitors'
