from django.db import models
from datetime import date

from schedules.models import Schedule
from residents.models import Resident

class HutVisitor(models.Model):
    name = models.CharField(max_length=255, blank=False)
    date = models.DateTimeField(auto_now_add=False, default=date.today)
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id',)
    
    def __str__(self) -> str:
        return f"{self.name}, {self.date}"