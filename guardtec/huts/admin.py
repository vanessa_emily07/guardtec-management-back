from django.contrib import admin

from .models import Hut

admin.site.register(Hut)