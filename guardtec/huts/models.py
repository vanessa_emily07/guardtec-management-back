from django.db import models
from django.db.models.fields import CharField

from residential_group.models import ResidentialGroup

class Hut(models.Model):
    name = models.CharField(max_length=255, blank=False)
    residential_group = models.ForeignKey(ResidentialGroup, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id', )

    def __str__(self):
        return self.name