from django.contrib import admin

from .models import IdentifierType

admin.site.register(IdentifierType)