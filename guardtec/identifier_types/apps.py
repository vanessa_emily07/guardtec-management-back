from django.apps import AppConfig


class IdentifierTypesConfig(AppConfig):
    name = 'identifier_types'
