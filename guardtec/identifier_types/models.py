from django.db import models
from django.db.models.fields import CharField

class IdentifierType(models.Model):
    name = CharField(max_length=50, blank=False)

    class Meta:
        ordering = ('name',)

    def __str__(self) -> str:
        return self.name