from django.db import models

from accounts.models import Administrator

class Individual(models.Model):
    RFC = models.CharField(max_length=14, blank=False, null=False)
    phone_number = models.CharField(max_length=14, blank=False, null=False)
    administrator = models.OneToOneField(Administrator, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.RFC}, {self.phone_number}"