from django.contrib import admin

from .models import LegalEntity

admin.site.register(LegalEntity)