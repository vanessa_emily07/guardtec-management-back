from django.db import models

from accounts.models import Administrator

class LegalEntity(models.Model):
    registered_name = models.CharField(max_length=255, blank=False, null=False)
    trade_name = models.CharField(max_length=255, blank=False, null=False)
    administrator = models.OneToOneField(Administrator, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.registered_name}, {self.trade_name}"