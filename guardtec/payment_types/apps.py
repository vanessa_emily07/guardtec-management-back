from django.apps import AppConfig


class PaymentTypesConfig(AppConfig):
    name = 'payment_types'
