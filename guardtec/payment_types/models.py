from django.db import models
from django.db.models.fields import CharField

class PaymentType(models.Model):
    name = CharField(max_length=50, blank=False)

    def __str__(self) -> str:
        return self.name