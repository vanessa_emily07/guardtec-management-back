from django.db import models

from payment_types.models import PaymentType

class Payment(models.Model):
    amount = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    date = models.DateField()
    payment_type = models.ForeignKey(PaymentType, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.amount}, {self.date}, {self.payment_type}"