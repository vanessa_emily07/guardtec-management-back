from django.contrib import admin

from .models import PedestrianAccess

admin.site.register(PedestrianAccess)