from django.apps import AppConfig


class PedestrianAccessConfig(AppConfig):
    name = 'pedestrian_access'
