from django.db import models

from residential_group.models import ResidentialGroup

class PedestrianAccess(models.Model):
    name = models.CharField(max_length=255, blank=False)
    residential_group = models.ForeignKey(ResidentialGroup, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.name}, {self.residential_group}"