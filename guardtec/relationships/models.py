from django.db import models
from django.db.models.fields import CharField

class Relationship(models.Model):
    name = models.CharField(max_length=45, blank=False)

    class Meta:
        ordering = ('name',)

    def __str__(self) -> str:
        return self.name