from django.contrib import admin

from .models import ResidenceStatus

admin.site.register(ResidenceStatus)