from django.apps import AppConfig


class ResidenceStatusConfig(AppConfig):
    name = 'residence_status'
