from rest_framework import serializers

from .models import ResidenceStatus

class ResidenceStatusSerializer(serializers.ModelSerializer):
    """Serializing residence status"""

    class Meta:
        model = ResidenceStatus
        fields = ('id', 'name')

class SaveResidenceStatusSerializer(serializers.ModelSerializer):
    """
    Serializes a residence status object.
    """

    class Meta:
        model = ResidenceStatus
        fields = ('id', 'name')
        extra_kwargs = {
            'name': { 'required': True, 'max_length': 45, 'min_length': 5 }
        }

    def create(self, validated_data):
        """Create and return a residence object"""

        residence_status = ResidenceStatus.objects.create(
            name = validated_data['name']
        )

        serialized = ResidenceStatusSerializer(residence_status)
        return serialized.data