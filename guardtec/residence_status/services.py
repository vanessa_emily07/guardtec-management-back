from .models import ResidenceStatus

def get_residence_status_by_id(residence_status_id):
    """Get residence status object by identifier"""
    
    try:
        return ResidenceStatus.objects.filter(pk=residence_status_id).get() 
    except:
        return None