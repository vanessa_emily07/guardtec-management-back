from django.urls import path, include

from residence_status import views

urlpatterns = [
    path('residence-status', views.ResidenceStatusAPIView.as_view())
]