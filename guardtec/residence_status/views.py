from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import SaveResidenceStatusSerializer, ResidenceStatusSerializer
from .models import ResidenceStatus

class ResidenceStatusAPIView(generics.GenericAPIView):
    """
    Residence status API View
    """

    permission_classes = [IsAuthenticated]
    serializer_class = SaveResidenceStatusSerializer

    def get(self, request):
        """Return a list of objects of the catalog Residence Status"""

        residence_status = ResidenceStatus.objects.all()
        serializer = ResidenceStatusSerializer(residence_status, many=True)

        return Response({
            "residence_status": serializer.data
        })

    def post(self, request):
        """
        Create a new element in the residence status catalog
        """
        serializers = self.serializer_class(data=request.data)

        if serializers.is_valid():
            name = serializers.validated_data.get('name')
            residence_status = serializers.save()
            
            return Response({
                'message': 'Residence status created successfully',
                'residence_status': residence_status
            })
        else:
            return Response(
                serializers.errors,
                status=status.HTTP_400_BAD_REQUEST
            )