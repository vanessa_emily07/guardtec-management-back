# Generated by Django 3.1.6 on 2021-02-16 03:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residences', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='residence',
            name='id',
            field=models.CharField(max_length=100, primary_key=True, serialize=False),
        ),
    ]
