# Generated by Django 3.1.6 on 2021-02-18 22:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('visiting_status', '0001_initial'),
        ('residence_status', '0001_initial'),
        ('residences', '0007_auto_20210218_2003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='residence',
            name='residence_status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='residence_status.residencestatus'),
        ),
        migrations.AlterField(
            model_name='residence',
            name='visiting_status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='visiting_status.visitingstatus'),
        ),
    ]
