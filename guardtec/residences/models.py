from django.db import models
from datetime import date

from residential_group.models import ResidentialGroup
from visiting_status.models import VisitingStatus
from residence_status.models import ResidenceStatus
from application_access.models import ApplicationAccess

class Residence(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    street = models.CharField(max_length=40, blank=False, null=False)
    external_number = models.PositiveIntegerField()
    is_active =  models.BooleanField(default=False) 
    kind_of_residence = models.CharField(max_length=45, blank=False, null=False)
    family_name = models.CharField(max_length=150, blank=False, null=False)
    internal_number = models.PositiveIntegerField()
    created_at = models.DateField(auto_now=True)
    created_user = models.CharField(max_length=255, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now_add=False, default=date.today)
    updated_user = models.CharField(max_length=255)
    residential_group = models.ForeignKey(ResidentialGroup, on_delete=models.CASCADE)
    residence_status = models.ForeignKey(ResidenceStatus, on_delete=models.CASCADE)
    visiting_status = models.ForeignKey(VisitingStatus, on_delete=models.CASCADE)
    application_access = models.ManyToManyField(ApplicationAccess)

    class Meta:
        ordering = ('id', )

    def __str__(self):
        return f"{self.id}, {self.street}, {self.is_active}, {self.external_number}, {self.kind_of_residence}, {self.internal_number}"