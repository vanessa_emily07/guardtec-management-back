from rest_framework import serializers

from application_access.serializers import ApplicationAccessSerializer
from residential_group.serializers import ResidentialGroupSerializer
from residence_status.serializers import ResidenceStatusSerializer
from visiting_status.serializers import VisitingStatusSerializer

from .models import Residence
from .services import is_number_valid

class LoadResidenceSerializer(serializers.Serializer):
    """Serializes to load a file"""

    file = serializers.FileField(allow_empty_file=False)

    class Meta:
        fields = ['file',]

class ResidencesSerializer(serializers.ModelSerializer):
    """Serializes a residence object."""

    id = serializers.CharField(min_length=4)
    street = serializers.CharField(min_length=5, max_length=40)
    external_number = serializers.IntegerField()
    kind_of_residence = serializers.CharField(min_length=5, max_length=40)
    internal_number = serializers.IntegerField()
    family_name = serializers.CharField()
    created_user = serializers.CharField()
    is_active = serializers.BooleanField()
    residential_group = ResidentialGroupSerializer()
    residence_status = ResidenceStatusSerializer()
    visiting_status = VisitingStatusSerializer()
    application_access = ApplicationAccessSerializer(many=True, read_only=True)

    class Meta:
        model = Residence
        fields = ('id', 'street', 'external_number', 'kind_of_residence', 'internal_number', 'is_active','created_user', 'family_name',
                    'residential_group', 'residence_status', 'visiting_status', 'application_access' )
        extra_kwargs = {
            'street': { 'min_length': 5, 'max_length': 40 },
            'kind_of_residence': { 'min_length': 5, 'max_length': 45 },
        }

class ResidencesFileSerializer(serializers.ModelSerializer):
    """Serializes a residence object to generate a residence report."""

    id = serializers.CharField(min_length=4)
    is_active = serializers.BooleanField()
    family_name = serializers.CharField()
    
    class Meta:
        model = Residence
        fields = ('id', 'is_active', 'family_name')


class ResidenceDetailSerializer(serializers.ModelSerializer):
    """Serializes a residence object."""

    id = serializers.CharField(min_length=4)
    family_name = serializers.CharField()
    
    class Meta:
        model = Residence
        fields = ('id', 'family_name')


class EditResidencesSerializer(serializers.ModelSerializer):
    """Serializes a residence object to be edited"""

    street = serializers.CharField(min_length=5, max_length=40)
    family_name = serializers.CharField(min_length=4, max_length=150)
    external_number = serializers.IntegerField()
    kind_of_residence = serializers.CharField(min_length=5, max_length=40)
    internal_number = serializers.IntegerField()
    residence_status = ResidenceStatusSerializer()
    application_access = ApplicationAccessSerializer(many=True)

    class Meta:
        model = Residence
        fields = ('street', 'external_number', 'family_name','kind_of_residence', 'internal_number', 'residence_status', 'application_access')
        extra_kwargs = {
            'street': { 'min_length': 5, 'max_lenfth': 40 },
            'kind_of_residence': { 'min_length': 5, 'max_lenfth': 45 },
        }
    
    def validate_external_number(self, value):
        try:
            is_number_valid(value)
        except TypeError:
            raise serializers.ValidationError("The external number must be an integer")
        return value

    def validate_internal_number(self, value):
        try:
            is_number_valid(value)
        except TypeError:
            raise serializers.ValidationError("The internal number must be an integer")
        return value

    def update(self, instance, validated_data, current_user_name, residence_status, application_access):
        for existing_app_access in instance.application_access.all():
            instance.application_access.remove(existing_app_access)
        
        instance.street = validated_data.get('street', instance.street)
        instance.external_number = validated_data.get('external_number', instance.external_number)
        instance.kind_of_residence = validated_data.get('kind_of_residence', instance.external_number)
        instance.internal_number = validated_data.get('internal_number', instance.internal_number)
        instance.family_name = validated_data.get('family_name', instance.family_name)
        if residence_status != None:
            instance.residence_status = residence_status
        instance.updated_user = validated_data.get(current_user_name, instance.updated_user)
        instance.save()

        if len(application_access) > 0:
            for app_access in application_access:
                instance.application_access.add(app_access)
                instance.save()
        
        return instance