from .models import Residence

def identifier_exist(residence_id):
    """Return True if the residence exists otherwise return False"""
    
    return Residence.objects.filter(id=residence_id).exists()
        
def get_residence_by_id(residence_id):
    """get the residence object if exists otherwise return none"""
    
    try:
        return Residence.objects.filter(pk=residence_id).get()
    except:
        return None

def is_number_valid(num):
    """
    Validate if a number is an integer or not, 
    in case it is not a integer, it rises an error
    """
    
    try:
        int(num)
    except ValueError:
        raise TypeError("Only Integer are allowed")