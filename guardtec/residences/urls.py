from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import ResidenceDetailAPIView, AllResidencesAPIView, LoadResidenceAPIView

urlpatterns = [
    path('residences/file-upload/', LoadResidenceAPIView.as_view()),
    path('residences', AllResidencesAPIView.as_view()),
    path('residences/<str:pk>', ResidenceDetailAPIView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)