from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from django.http import Http404
from rest_framework.mixins import UpdateModelMixin
from django.db.models import Q
import pandas as pd
from drf_yasg.utils import swagger_auto_schema

from residence_status.services import get_residence_status_by_id
from visiting_status.services import get_first_visiting_status
from application_access.services import get_application_access_by_id
from residential_group.services import get_residential_group_by_id
from accounts.services import get_residential_group_by_administrator_id

from .serializers import ResidencesSerializer, EditResidencesSerializer
from .models import Residence
from .services import identifier_exist, is_number_valid
from .paginations import CustomPagination        

NUMBER_OF_COLUMNS_RESIDENCE_FILE = 8

class AllResidencesAPIView(generics.ListAPIView):
    """
    Get a pagination and a list of all residence which they can filter by residence identifier or street name sending the query param 'search'
    """
    
    permission_classes = [IsAuthenticated]
    serializer_class = ResidencesSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        """
        Optinally filter against id and street query parameters in the URL. 
        """
        
        user = self.request.user
        
        queryset = Residence.objects.filter(residential_group = user.residential_group)
        search = self.request.query_params.get('search', None)
        
        if search is not None:
            queryset = queryset.filter(Q(id__icontains=search) | Q(street__icontains=search))
        
        return queryset


class LoadResidenceAPIView(APIView):
    """Residence API View"""

    permission_classes = [IsAuthenticated]
    parser_classes = [MultiPartParser, FormParser]

    @swagger_auto_schema(operation_description="Endpoint to load an excel file to create residences. The file is sending a form data with the name 'file'",
                    responses={
                        422: 'This error can occur in the following cases: Invalid format file. The file must be an excel file. If the excel has more or less columns or if the residential group and visiting status catalog does not have any element',
                        200: 'A list of errors  that were found while reading the file '})
    def post(self, request, *args, **kwargs):
        """Load file to create multiple residences."""
        
        file_uploaded = request.FILES.get('file')
        file = file_uploaded.file
        
        try:
            data = pd.read_excel(file)
            
            errors = []    
            number_columns =len(data.columns)
            
            if number_columns != NUMBER_OF_COLUMNS_RESIDENCE_FILE:
                return Response( {'message: ' 'The excel file does not have the correct number of columns'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            current_user_name = request.user.name + ' ' + request.user.last_name
            residential_group_id = get_residential_group_by_administrator_id(request.user.id)
            residential_group = get_residential_group_by_id(residential_group_id)
            visiting_status = get_first_visiting_status()
            
            if residential_group == None:
                return Response(
                    {'message:' 'Residential group does not exist'},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
            
            if  visiting_status == None:
                return Response(
                    {'message:' 'Visiting status does not exist'},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )     

            for i, row in data.iterrows():

                if row[0] == None:
                    errors.append({ 'id': "", 'message': 'El identificador de la residencia no puede estar vacio.'})
                    continue

                if row[1] == None:
                    errors.append({ 'id': row[0], 'message': 'La calle de la residencia no puede estar vacio.'})
                    continue

                if row[2] == None:
                    errors.append({ 'id': row[0], 'message': 'El número exterior de la residencia no puede estar vacio.'})
                    continue

                if row[3] == None:
                    errors.append({ 'id': row[0], 'message': 'No puede estar vacio el tipo de residencia (Edificio o Torre).'})
                    continue

                if row[4] == None:
                    errors.append({ 'id': row[0], 'message': 'El número interior de la residencia no puede estar vacio.'})
                    continue

                if row[6] == None:
                    errors.append({ 'id': row[0], 'message': 'El tipo de acceso no puede estar vacio.'})
                    continue

                if row[7] == None:
                    errors.append({ 'id': row[0], 'message': 'El estatus de la residencia no puede estar vacio.'})
                    continue
                
                if identifier_exist(row[0]):
                    errors.append({ 'id': row[0], 'message': 'El identificador de la residencia ya existe' })
                    continue
                
                residence_status = get_residence_status_by_id(row[7])
                
                if residence_status == None:
                    errors.append({ 'id': row[0], 'message': 'El estatus de la residencia no existe' })
                    continue
                
                application_access = get_application_access_by_id(row[6])
                if application_access == None:
                    errors.append({ 'id': row[0], 'message': 'El acceso a la aplicación no existe' })
                    continue
                
                if row[5] == None:
                    errors.append({ 'id': row[0], 'message': 'El nombre de la familia de la residencia no puede estar vacio.'})
                    continue

                try:
                    is_number_valid(row[2])
                    is_number_valid(row[4])
                except TypeError:
                    errors.append({ 'id': row[0], 'message': 'El número interior y exterior deben ser números' })
                    continue
                
                residence = Residence(id =row[0], street = row[1], external_number = row[2], family_name= row[5], kind_of_residence = row[3], internal_number = row[4], created_user = current_user_name, residential_group = residential_group, residence_status = residence_status, visiting_status = visiting_status, is_active = True)
                residence.save()
                residence.application_access.add(application_access)
            
            return Response({
                'message': 'Residence created successfully',
                'errors': errors
            })

        except ValueError as e:
            return Response(
                {'message: ' 'Invalid format file. The file must be an excel file'},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


class ResidenceDetailAPIView(APIView, UpdateModelMixin):
    """Residence Detail API VIew"""
    
    permission_classes = [IsAuthenticated]
    
    def get_object(self, pk, residential_group):
        """Look for a residence by identifier otherwise return 404 error"""

        try:
            return Residence.objects.get(pk=pk, residential_group=residential_group)
        except Residence.DoesNotExist:
            raise Http404

    @swagger_auto_schema(responses={
                        400: 'The residence identifier does not exist',
                        200: ResidencesSerializer})
    def get(self, request, pk, format=None):
        """Get a residence object by identifier"""

        user = self.request.user
        residence = self.get_object(pk, user.residential_group)
        serializer = ResidencesSerializer(residence)
        return Response(serializer.data)

    @swagger_auto_schema(responses={
                        400: 'The residence identifier does not exist',
                        422: 'The client is sending data that does not exist in catalogs',
                        200: ResidencesSerializer})
    def put(self, request, pk, format=None):
        """Update a residence by its identifier."""

        user = self.request.user
        residence = self.get_object(pk, user.residential_group)
        current_user_name = request.user.name + ' ' + request.user.last_name
        

        serializer = EditResidencesSerializer(residence, 
                    data={ 'street': request.data.get('street'), 'external_number': request.data.get('external_number'), 'family_name': request.data.get('family_name'),
                            'kind_of_residence': request.data.get('kind_of_residence'), 'internal_number': request.data.get('internal_number')}, partial=True)
        
        residence_status = None
        if request.data.get('residence_status', {}).get('id') != None:
            residence_status = get_residence_status_by_id(request.data.get('residence_status', {}).get('id'))
            
            if residence_status == None:
                return Response(
                    {'message:' 'Residence status does not exist'},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        application_access_data = []
        if request.data.get('application_access', {}) != None:
            for app_access in request.data.get('application_access', {}):
                app_access_id = app_access.get('id')
                data = get_application_access_by_id(app_access_id)
                
                if data == None:
                    return Response(
                        {'message:' f' Application access with identifier {app_access_id} does not exist'},
                        status=status.HTTP_422_UNPROCESSABLE_ENTITY
                    )
                application_access_data.append(data)

        if serializer.is_valid():
            serializer.update(residence, serializer.validated_data, current_user_name, residence_status, application_access_data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)