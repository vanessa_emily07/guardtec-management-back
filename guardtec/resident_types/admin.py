from django.contrib import admin

from .models import ResidentType

admin.site.register(ResidentType)