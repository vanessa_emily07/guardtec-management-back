from django.apps import AppConfig


class ResidentTypesConfig(AppConfig):
    name = 'resident_types'
