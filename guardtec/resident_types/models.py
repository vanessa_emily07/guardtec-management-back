from django.db import models

class ResidentType(models.Model):
    name = models.CharField(max_length=45, blank=False)

    class Meta:
        ordering = ('name',)
    
    def __str__(self) -> str:
        return self.name