from rest_framework import serializers

from .models import ResidentType

class ResidentTypeSerializer(serializers.ModelSerializer):
    """
    Serializing Resident Types
    """

    class Meta:
        model = ResidentType
        fields = ('id', 'name')

class SaveResidentTypeSerializer(serializers.ModelSerializer):
    """
    Serializes a resident type object to save it.
    """

    class Meta:
        model = ResidentType
        fields = ('id', 'name')
        extra_kwargs = {
            'name': { 'required': True, 'max_length': 45, 'min_length': 3 }
        }

    def create(self, validated_data):
        """Create and return a resident type object"""

        resident_type = ResidentType.objects.create(
            name = validated_data['name']
        )

        serialized = ResidentTypeSerializer(resident_type)

        return serialized.data
    