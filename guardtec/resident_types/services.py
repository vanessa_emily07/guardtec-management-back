from django.db.models import Q

from .models import ResidentType

def get_resident_type_by_id(resident_type_id):
    """Get resident type object if exists otherwise return none."""

    try:
        return ResidentType.objects.filter(pk=resident_type_id).get()
    except:
        return None

def get_administrator_or_contact_resident_type():
    """Get administrator or contact resident type"""

    try:
        return ResidentType.objects.filter(Q(name='Administrador') | Q(name='Contacto')).all()
    except:
        return None