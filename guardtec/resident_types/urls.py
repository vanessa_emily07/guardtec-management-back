from django.urls import path

from resident_types import views

urlpatterns = [
    path('resident-types', views.ResidentTypeAPIView.as_view())
]