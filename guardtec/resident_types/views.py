from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response

from .serializers import SaveResidentTypeSerializer, ResidentTypeSerializer
from .models import ResidentType

class ResidentTypeAPIView(generics.GenericAPIView):
    """
    Resident Type API View
    """

    permission_classes = [IsAuthenticated]
    serializer_class = SaveResidentTypeSerializer

    def get(self, request):
        """Return a list of resident types catalog"""

        resident_types = ResidentType.objects.all()
        serializer = ResidentTypeSerializer(resident_types, many=True)

        return Response({
            "residents_types": serializer.data
        })

    def post(self, request):
        """
        Create a new element in resident type catalog
        """

        serializers = self.serializer_class(data=request.data)

        if serializers.is_valid():
            name = serializers.validated_data.get('name')
            resident_type = serializers.save()

            return Response({
                'message': 'Resident type created successfully',
                'resident_type': resident_type
            })
        else:
            return Response(
                serializers.erros,
                status=status.HTTP_400_BAD_REQUEST
            )
