from django.contrib import admin

from .models import ResidentialGroup

admin.site.register(ResidentialGroup)