from django.apps import AppConfig


class ResidentialGroupConfig(AppConfig):
    name = 'residential_group'
