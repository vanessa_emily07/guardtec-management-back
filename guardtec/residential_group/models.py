from django.db import models

from payments.models import Payment

class ResidentialGroup(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    city = models.CharField(max_length=45, blank=False, null=False)
    address = models.CharField(max_length=255, blank=False, null=False)
    cp = models.CharField(max_length=5, blank=False, null=False)
    number_homes = models.PositiveIntegerField(null=False, blank=False)
    number_vehicular_access = models.PositiveIntegerField()
    number_pedestrian_access = models.PositiveIntegerField()
    access_type = models.CharField(max_length=50, blank=False)
    contact_name = models.CharField(max_length=255, blank=False)
    payments = models.ManyToManyField(Payment)
    
    def __str__(self):
        return f"{self.name}, {self.city}, {self.address}, {self.cp}, {self.contact_name}, {self.access_type}" 
