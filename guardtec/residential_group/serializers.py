from rest_framework import serializers

from .models import ResidentialGroup

class ResidentialGroupSerializer(serializers.ModelSerializer):
    """Residential Group Serializer"""

    class Meta:
        model = ResidentialGroup
        fields = ('id', 'name')
