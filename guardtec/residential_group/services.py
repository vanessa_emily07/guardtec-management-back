from .models import ResidentialGroup

def get_residential_group_by_id(residential_group_id):
    """Get a residential group by administrator identifier"""

    try:
        return ResidentialGroup.objects.filter(pk=residential_group_id).get()
    except:
        return None
