# Generated by Django 3.1.6 on 2021-02-25 21:08

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('residences', '0009_auto_20210225_2108'),
        ('resident_types', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resident',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('phone_number', models.CharField(max_length=15)),
                ('email', models.CharField(max_length=255, unique=True)),
                ('token_device', models.CharField(max_length=255)),
                ('created_at', models.DateField(auto_now=True)),
                ('created_user', models.CharField(max_length=255)),
                ('updated_at', models.DateTimeField(default=datetime.date.today)),
                ('updated_user', models.CharField(max_length=255)),
                ('is_active', models.BooleanField(default=False)),
                ('has_access', models.BooleanField(default=False)),
                ('residence', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='residences.residence')),
                ('resident_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='resident_types.residenttype')),
            ],
            options={
                'ordering': ('id',),
            },
        ),
    ]
