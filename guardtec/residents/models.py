from django.db import models
from datetime import date

from resident_types.models import ResidentType
from residences.models import Residence

class Resident(models.Model):
    name = models.CharField(max_length=150, blank=False)
    last_name = models.CharField(max_length=150, blank=False)
    phone_number = models.CharField(max_length=15, blank=False)
    email = models.CharField(max_length=255, unique=True, blank=False)
    password = models.CharField(max_length=100, null=True)
    token_device = models.CharField(max_length=255)
    verification_code = models.PositiveIntegerField(null=True)
    created_at = models.DateField(auto_now=True)
    created_user = models.CharField(max_length=255, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now_add=False, default=date.today)
    updated_user = models.CharField(max_length=255)  
    is_active =  models.BooleanField(default=False) 
    has_access = models.BooleanField(default=False)
    residence = models.ForeignKey(Residence, on_delete=models.CASCADE)
    resident_type = models.ForeignKey(ResidentType,on_delete=models.CASCADE)

    class Meta:
        ordering = ('id', )

    def __str__(self):
        return f"{self.name},{self.last_name} ,{self.phone_number}, {self.email}, {self.is_active}, {self.has_access}, {self.resident_type}"