from rest_framework import serializers

from resident_types.serializers import ResidentTypeSerializer
from residences.serializers import ResidenceDetailSerializer

from .models import Resident
from .services import is_number_valid, is_email_valid, email_exists

class LoadResidentsSerializer(serializers.Serializer):
    """Serializes a file in order to create multiple residents."""

    file = serializers.FileField(allow_empty_file=False)

    class Meta:
        fields = ['file',]

class ResidentSerializer(serializers.ModelSerializer):
    """Serializes a list of residents."""

    id = serializers.CharField()
    name = serializers.CharField()
    last_name = serializers.CharField()
    phone_number = serializers.CharField()
    email = serializers.CharField()
    is_active = serializers.BooleanField()
    resident_type = ResidentTypeSerializer()
    residence = ResidenceDetailSerializer()

    class Meta:
        model = Resident
        fields = ('id', 'name', 'last_name','phone_number', 'email','is_active', 'resident_type', 'residence')

class ResidentDetailSerializer(serializers.ModelSerializer):
    """Serializes a resident object"""

    name = serializers.CharField(min_length=5, max_length=150)
    last_name = serializers.CharField(min_length=5, max_length=150)
    phone_number = serializers.CharField(min_length=8, max_length=15)
    email = serializers.CharField(max_length=255)
    resident_type = ResidentTypeSerializer()

    class Meta:
        model = Resident
        fields = ('name', 'last_name','phone_number', 'email', 'resident_type')


    def validate_phone_number(self, value):
        try:
            is_number_valid(value)
        except TypeError:
            raise serializers.ValidationError("The phone number must be a number")
        return value
    
    
    def validate_email(self, value):
        if not is_email_valid(value):
            raise serializers.ValidationError("The email address is not valid")

        return value

    def partial_update(self, instance, validated_data, resident_type, current_user):
        instance.name = validated_data.get('name', instance.name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.email = validated_data.get('email', instance.email)
        instance.updated_user = current_user
        
        if resident_type != None:
            instance.resident_type = resident_type
        instance.save()
        
        return instance
        
