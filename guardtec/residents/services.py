import re

from .models import Resident

EMAIL_REGEX = '[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+'
NUMBER_REGEX = '[0-9]+'

def is_email_valid(email):
    if(re.search(EMAIL_REGEX, email)):
        return True
    else:
        return False

def is_number_valid(num):
    try:
        int(num)
    except ValueError:
        raise TypeError("Only Integer are allowed")

def email_exists(email):
    """Return True if the email exists otherwise return none"""
    
    return Resident.objects.filter(email=email).exists()