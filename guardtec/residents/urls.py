from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import LoadResidentsAPIView, ListResidents, ResidentDetailAPI


urlpatterns = [
    path('residents/file-upload/', LoadResidentsAPIView.as_view()),
    path('residents', ListResidents.as_view()),
    path('residents/<int:pk>', ResidentDetailAPI.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)