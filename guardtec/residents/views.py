from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from django.http import Http404
from rest_framework.mixins import UpdateModelMixin
from drf_yasg.utils import swagger_auto_schema

import pandas as pd

from residences.services import get_residence_by_id
from resident_types.services import get_resident_type_by_id
from residences.models import Residence

from .services import is_email_valid, is_number_valid, email_exists
from .models import Resident
from .serializers import ResidentSerializer, ResidentDetailSerializer
from .paginations import CustomPagination

NUMBER_OF_COLUMNS_RESIDENCE_FILE = 6

class LoadResidentsAPIView(APIView):
    """Allow to load an excel file to create residents"""

    permission_classes = [IsAuthenticated]
    parser_classes = [MultiPartParser, FormParser]

    @swagger_auto_schema(operation_description="Endpoint to load an excel file to create residents. The file is sending in form data with the name 'file'",
                    responses={
                        422: 'This error can occur in the following cases: Invalid format file. The file must be an excel file. If the excel has more or less columns or if the residential group and visiting status catalog does not have any element',
                        200: 'A list of errors  that were found while reading the file '})
    def post(self, request, *args, **kwargs):
        """Allow to load an excel file to create multiple residents."""
        
        file_uploaded = request.FILES.get('file')
        file = file_uploaded.file

        user = self.request.user
        queryset_residences = Residence.objects.filter(residential_group = user.residential_group, is_active=True)
        list_ids = [residence.id for residence in queryset_residences]
        
        try:
            data = pd.read_excel(file)
            errors = []

            number_columns =len(data.columns)
            if number_columns != NUMBER_OF_COLUMNS_RESIDENCE_FILE:
                return Response( {'message: ' 'The excel file does not have the correct number of columns'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            current_user_name = request.user.name + ' ' + request.user.last_name
            
            for i, row in data.iterrows():

                if row[0] == None:
                    errors.append({ 'name': row[1], 'message': 'El identificador de la residencia no puede estar vacio.'})
                    continue
                
                if not (row[0] in list_ids):
                    errors.append({ 'name': row[1], 'message': f'El identificador de la residencia {row[0]} no existe' })
                    continue
                
                if row[1] == None:
                    errors.append({ 'name': "", 'message': 'El nombre del residente no puede estar vacio.'})
                    continue
                
                if row[2] == None:
                    errors.append({ 'name': row[1], 'message': 'El apellido del residente no puede estar vacio.'})
                    continue

                if row[3] == None:
                    errors.append({ 'name': row[1], 'message': 'El tipo del residente no puede estar vacio.'})
                    continue
                
                resident_type = get_resident_type_by_id(row[3])

                if resident_type == None:
                    errors.append({ 'name': row[1], 'message': f'El tipo del residente {row[3]} no existe.'})
                    continue

                if row[4] == None:
                    errors.append({ 'name': row[1], 'message': 'El teléfono del residente no puede estar vacio.'})
                    continue

                if row[5] == None:
                    errors.append({ 'name': row[1], 'message': 'El correo electrónico del residente no puede estar vacio.'})
                    continue

                if not is_email_valid(row[5]):
                    errors.append({ 'name': row[1], 'message': f'El correo electrónico {row[5]} no es válido.'})
                    continue
                
                if email_exists(row[5]):
                    errors.append({ 'name': row[1], 'message': f'El correo electrónico {row[5]} ya existe.'})
                    continue

                try:
                   is_number_valid(row[4])
                except TypeError:
                    errors.append({ 'id': row[0], 'message': 'El número de teléfono solo admite números' })
                    continue
                
                residence = get_residence_by_id(row[0])

                resident = Resident(name=row[1], last_name=row[2], phone_number = row[4], email= row[5], created_user=current_user_name, is_active=True, residence=residence, resident_type=resident_type)
                resident.save()

            return Response({
                'message': 'Residence created successfully',
                'errors': errors
            })
        
        except ValueError as e:
            return Response(
                {'message: ' 'Invalid format file. The file must be an excel file.'},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

class ListResidents(generics.ListAPIView):
    """Get a list of all residents optionally filter against id and name using uery parameters in the URL."""

    permission_classes = [IsAuthenticated]
    serializer_class = ResidentSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        """Get a list of all residents, optionally filter against id and name using uery parameters in the URL."""
        
        user = self.request.user
        
        queryset_residences = Residence.objects.filter(residential_group = user.residential_group, is_active=True)
        list_ids = [residence.id for residence in queryset_residences]
        
        queryset = Resident.objects.all()
        queryset = queryset.filter(residence__in=list_ids)
        
        id = self.request.query_params.get('id', None)
        name = self.request.query_params.get('name', None)

        if id is not None:
            queryset = queryset.filter(id__contains=id)
        
        if name is not None:
            queryset = queryset.filter(name__icontains=name)
        
        return queryset

class ResidentDetailAPI(APIView, UpdateModelMixin):
    """Resident detail API"""

    permission_classes = [IsAuthenticated]

    def get_object(self, pk, list_ids):
        """Look for a resident otherwise return 404 error"""

        try:
            return Resident.objects.get(pk=pk, residence__in=list_ids, is_active=True)
        except Resident.DoesNotExist:
            raise Http404
    
    @swagger_auto_schema(responses={ 400: 'The resident identifier does not exist', 200: ResidentDetailSerializer})
    def get(self, request, pk, format=None):
        """Get a resident by their identifier"""
        
        user = self.request.user
        queryset_residences = Residence.objects.filter(residential_group = user.residential_group, is_active=True)
        list_ids = [residence.id for residence in queryset_residences]
        
        resident = self.get_object(pk, list_ids)
        serializer = ResidentSerializer(resident)
        return Response(serializer.data)

    @swagger_auto_schema(responses={
                        400: 'The resident identifier does not exist',
                        422: 'The client is sending data that does not exist in catalogs',
                        200: ResidentDetailSerializer})
    def put(self, request, pk, format=None):
        """Endpoint to update a resident by the given identifier."""

        user = self.request.user
        queryset_residences = Residence.objects.filter(residential_group = user.residential_group, is_active=True)
        list_ids = [residence.id for residence in queryset_residences]
        
        resident = self.get_object(pk, list_ids)
        current_user_name = request.user.name + ' ' + request.user.last_name
        
        serializer = ResidentDetailSerializer(resident,data={ 'name': request.data.get('name',{}), 'last_name': request.data.get('last_name'),
                                                'phone_number': request.data.get('phone_number'), 'email': request.data.get('email')}, partial=True)
        
        resident_type = None
        if request.data.get('resident_type', {}).get('id') != None:
            
            resident_type = get_resident_type_by_id(request.data.get('resident_type', {}).get('id'))

            if resident_type == None:
                return Response (
                    { 'message': 'Resident type does not exist'},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
        
        if serializer.is_valid():
            serializer.partial_update(resident, serializer.validated_data, resident_type, current_user_name)
            return Response(serializer.data)
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)