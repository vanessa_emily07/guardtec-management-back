from rest_framework import serializers

class AccessReportSerializer(serializers.Serializer):
    """Access Report serializer for excel file"""

    id = serializers.CharField(required=True)
    family_name = serializers.CharField(required=True)
    entry_date = serializers.DateField(required=True)
    exit_date = serializers.DateField(required=True)

    class Meta:
        fields = ('id', 'family_name', 'entry_date', 'exit_date')