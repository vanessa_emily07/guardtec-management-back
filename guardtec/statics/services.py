import datetime

def is_dates_valid(initial_date, final_date):
    """Method to validate that the initial date is less than the end date"""

    y1, m1, d1 = split_date_text(initial_date)
    y2, m2, d2 = split_date_text(final_date)
    b1 = datetime.date(y1, m1, d1)
    b2 = datetime.date(y2, m2, d2)

    if b1 > b2:
        return False
    return True

def validate(date_text):
    """Method to validate date taxt has the following format: 'YYYY-MM-DD'"""
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")

def split_date_text(date_text):
    """Split string text to get the year, month and day"""
    return [int(x) for x in date_text.split('-')]
