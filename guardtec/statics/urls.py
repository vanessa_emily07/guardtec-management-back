from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import StaticsResidences, StaticsResidencesFile, StaticAccessFile

urlpatterns = [
    path('statics/residences', StaticsResidences.as_view(), name='statics-residences'),
    path('statics/residences/file', StaticsResidencesFile.as_view(), name ='statics-residences-file'),
    path('statics/access/file', StaticAccessFile.as_view(), name='statics-access-file')
]

urlpatterns = format_suffix_patterns(urlpatterns)