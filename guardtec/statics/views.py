from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from drf_renderer_xlsx.renderers import XLSXRenderer
from rest_pandas import PandasView, PandasExcelRenderer
from django.http import HttpResponse
import os

from residences.serializers import ResidencesSerializer, ResidencesFileSerializer
from residences.paginations import CustomPagination
from residences.models import Residence
from access.models import Access

from .serializers import AccessReportSerializer
from .services import validate, is_dates_valid

class StaticsResidences(generics.ListAPIView):
    """Get a report of all residences VIEW"""

    permission_classes = [IsAuthenticated]
    serializer_class = ResidencesSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        """Get a report for all residences by the residential of the user that has been authenticated"""

        user = self.request.user
        
        queryset = Residence.objects.filter(residential_group = user.residential_group)
        return queryset

class StaticsResidencesFile(PandasView):
    """Get an excel file of all residences that the user has been authenticated"""
    
    queryset = Residence.objects.all()

    def filter_queryset(self, qs): 
        user = self.request.user
        qs.filter(residential_group = user.residential_group)
        return qs
    
    serializer_class = ResidencesFileSerializer

    renderer_classes = [PandasExcelRenderer]

    def get_pandas_filename(self, request, format):
        if format in ('xls', 'xlsx'):
            return os.getenv("RESIDENCES_REPORT_FILENAME")  # Extension will be appended automatically
        else:
            return None

class StaticAccessFile(APIView):
    """Generate report of access"""

    renderer_classes = [XLSXRenderer, JSONRenderer]

    def get(self, request, *args, **kwargs):
        """
        Generate an excel file of all access by seding in query params the 'initial_date' and 'final_date' 
        and opionally the residence identifier by sending the 'residence_id' query params
        """

        residence_id = self.request.query_params.get('residence_id', None)
        initial_date = self.request.query_params.get('initial_date', None)
        final_date = self.request.query_params.get('final_date', None)
        
        if initial_date is None:
            return HttpResponse('Initial date must be provided', status=status.HTTP_400_BAD_REQUEST)
            
        if final_date is None:
            return HttpResponse('Final date must be provided', status=status.HTTP_400_BAD_REQUEST)
        
        try:
            validate(initial_date)
            validate(final_date)    
        except ValueError:
            return HttpResponse('Incorrect data format, should be YYYY-MM-DD', status=status.HTTP_400_BAD_REQUEST)

        if not is_dates_valid(initial_date, final_date):
            return HttpResponse('The initial date must be less than the final date', status=status.HTTP_400_BAD_REQUEST)
        
        # validate residence identifier exists by the current user  
        user = self.request.user
        queryset_residences = Residence.objects.filter(residential_group = user.residential_group)
        list_ids = [residence.id for residence in queryset_residences]
        
        if residence_id is not None and not residence_id in list_ids:
            return HttpResponse('Residence identifier does not exist', status=status.HTTP_400_BAD_REQUEST)
        
        if residence_id is not None:
            access_query = Access.objects.raw("""SELECT rs.id, rs.family_name, a.entry_date, a.exit_date 
                        FROM access_access as a inner join trust_visitors_trustvisitor as tv on a.trust_visitor_id = tv.id
                        inner join residents_resident as r on r.id = tv.resident_id 
                        inner join residences_residence as rs on rs.id = r.residence_id 
                        inner join residential_group_residentialgroup as rg on rg.id = rs.residential_group_id 
                        where rs.id = %s and entry_date = %s and exit_date = %s and rg.id=%s;""", [residence_id, initial_date, final_date, user.residential_group.id])
        else:
            access_query = Access.objects.raw("""SELECT rs.id, rs.family_name, a.entry_date, a.exit_date 
                        FROM access_access as a inner join trust_visitors_trustvisitor as tv on a.trust_visitor_id = tv.id
                        inner join residents_resident as r on r.id = tv.resident_id 
                        inner join residences_residence as rs on rs.id = r.residence_id 
                        inner join residential_group_residentialgroup as rg on rg.id = rs.residential_group_id 
                        where entry_date = %s and exit_date = %s and rg.id=%s;""", [initial_date, final_date, user.residential_group.id])
        
        serializer = AccessReportSerializer(list(access_query), many=True)
        return Response(data=serializer.data, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')