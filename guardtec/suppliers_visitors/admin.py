from django.contrib import admin

from .models import SupplierVisitor

admin.site.register(SupplierVisitor)