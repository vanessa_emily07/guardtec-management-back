from django.apps import AppConfig


class SuppliersVisitorsConfig(AppConfig):
    name = 'suppliers_visitors'
