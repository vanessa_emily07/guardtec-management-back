from django.contrib import admin

from .models import TrustVisitor

admin.site.register(TrustVisitor)