from django.apps import AppConfig


class TrustVisitorsConfig(AppConfig):
    name = 'trust_visitors'
