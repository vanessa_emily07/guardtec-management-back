from django.db import models

from residents.models import Resident
from relationships.models import Relationship

class TrustVisitor(models.Model):
    name = models.CharField(max_length=255, blank=False)
    phone_number = models.CharField(max_length=15, blank=False)
    picture_url = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)
    created_at = models.DateField(auto_now=True)
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    relationship = models.ForeignKey(Relationship, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id',)

    def __str__(self) -> str:
        return f"{self.name}, {self.phone_number}, {self.is_active}, {self.created_at}"