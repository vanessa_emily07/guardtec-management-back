from django.apps import AppConfig


class VigilantsConfig(AppConfig):
    name = 'vigilants'
