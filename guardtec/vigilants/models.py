from django.db import models

from huts.models import Hut

class Vigilant(models.Model):
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=False)
    hut = models.ForeignKey(Hut, on_delete=models.CASCADE)
    password = models.CharField(max_length=100)

    class Meta:
        ordering = ('id',)
    
    def __str__(self) -> str:
        return f"{self.email}, {self.name}, {self.last_name}"