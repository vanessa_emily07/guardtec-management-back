from django.contrib import admin

from .models import VisitingStatus

admin.site.register(VisitingStatus)