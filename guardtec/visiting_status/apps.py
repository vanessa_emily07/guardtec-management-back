from django.apps import AppConfig


class VisitingStatusConfig(AppConfig):
    name = 'visiting_status'
