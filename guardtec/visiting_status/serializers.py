from rest_framework import serializers

from visiting_status.models import VisitingStatus

class VisitingStatusSerializer(serializers.ModelSerializer):
    """
    Serializing visiting status
    """

    class Meta:
        model = VisitingStatus
        fields = ('id', 'name')

class SaveVisitingStatusSerializer(serializers.ModelSerializer):
    """
    Serializes a visiting status object.
    """

    class Meta:
        model = VisitingStatus
        fields = ('id','name')
        extra_kwargs = {
            'name': { 'required': True, 'max_length': 45, 'min_length': 3 }
        }

    def create(self, validated_data):
        """Create and return a visiting status object"""
        
        visiting_status = VisitingStatus.objects.create(
            name = validated_data['name']
        ) 
        serialized = VisitingStatusSerializer(visiting_status)
        return serialized.data

    def update(self, instance, validated_data):

        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance