from .models import VisitingStatus

def get_first_visiting_status():
    """Get the first visiting status object"""

    visiting_status= VisitingStatus.objects.first()

    if not visiting_status:
        return None

    return visiting_status

def get_visiting_status_by_id(visiting_status_id):
    """Get visiting status by identifier"""

    try:
        return VisitingStatus.objects.filter(pk=visiting_status_id).get()
    except:
        return None