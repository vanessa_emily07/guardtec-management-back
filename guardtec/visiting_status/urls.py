from django.urls import path

from visiting_status import views

urlpatterns = [
    path('visiting-status', views.VisitingStatusAPIView.as_view())
]