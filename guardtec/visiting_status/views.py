from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response

from visiting_status import serializers
from .models import VisitingStatus

class VisitingStatusAPIView(generics.GenericAPIView):
    """
    Visiting status API View
    """

    permission_classes = [IsAuthenticated]
    serializer_class = serializers.SaveVisitingStatusSerializer

    def get(self, request):
        """Returns a list of all objects of the Visiting status catalog"""

        visiting_status = VisitingStatus.objects.all()
        serializer = serializers.VisitingStatusSerializer(visiting_status, many=True)
        return Response({
            "visiting_status": serializer.data
        })

    def post(self, request):
        """
        Create a new element in the visiting status catalog
        """
        serializers = self.serializer_class(data=request.data)

        if serializers.is_valid():
            name = serializers.validated_data.get('name')
            visiting_status = serializers.save()
            return Response({
                'message': 'Visiting status created successfully',
                'visiting_status': visiting_status
            })
        else:
            return Response(
                serializers.errors, 
                status=status.HTTP_400_BAD_REQUEST
            )
